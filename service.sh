#!/bin/bash
#
# service - Starts and stops IRC bot.

BOT_NAME=bot-name
BOT_USER=ircbot
BOT_HOME=/home/ircbot/$BOT_NAME
BOT_COMMAND=$BOT_HOME/bot.sh
PID_FILE=/var/run/ircbot/${BOT_NAME}.pid
LOG_FILE=/var/log/ircbot/${BOT_NAME}.log

set -x

start_bot() {
    if [ -f $PID_FILE ]; then
        PID=`cat ${PID_FILE}`
        echo "${BOT_NAME} is already running (pid: ${PID})"
    else
        echo "Starting ${BOT_NAME}..."
        /bin/su -p -s /bin/sh -c "nohup $BOT_COMMAND >> $LOG_FILE 2>&1 & echo "'$!'" > $PID_FILE" $BOT_USER
        PID=`cat ${PID_FILE}`
        
        if ps -p $PID > /dev/null
        then
			echo "${BOT_NAME} started successfully (pid: ${PID})"
		else
			echo "Failed to start ${BOT_NAME}"
			rm -f $PID_FILE || true
		fi
    fi
    return 0
}

stop_bot() {
    if [ -f $PID_FILE ]; then
        PID=`cat ${PID_FILE}`
        echo "Stopping ${BOT_NAME} (pid: ${PID})..."
        kill $PID
        rm -f $PID_FILE || true
    else
        echo "${BOT_NAME} is not running."
    fi
    return 0
}

case "$1" in
start)
    start_bot
    ;;
stop)
    stop_bot
    ;;
restart)
    stop_bot
    start_bot
    ;;
*)
    echo "Usage: $0 {start|stop|restart}"
    exit 1
esac
