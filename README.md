# IRC Bot Scripts
A set of scripts to manage and install independent IRC bots.

## Using the scripts
These scripts are just shells that can be applied to a variety of projects. Manual changes will need to be made to each script to set application-specific information.
